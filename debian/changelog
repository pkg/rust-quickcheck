rust-quickcheck (1.0.3-2+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 16 Jan 2025 17:22:50 +0100

rust-quickcheck (1.0.3-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Apr 2023 00:16:17 +0000

rust-quickcheck (1.0.3-2) unstable; urgency=medium

  * Team upload.
  * Package quickcheck 1.0.3 from crates.io using debcargo 2.4.4
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 05 Feb 2022 11:22:40 +0000

rust-quickcheck (1.0.3-1) experimental; urgency=medium

  * Package quickcheck 1.0.3 from crates.io using debcargo 2.5.0 (Closes: 1001656)

  [ Sylvestre Ledru ]
  * Team upload.
  * Package quickcheck 1.0.3 from crates.io using debcargo 2.5.0

 -- Peter Michael Green <plugwash@debian.org>  Sun, 23 Jan 2022 21:09:54 +0000

rust-quickcheck (0.9.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * add patch to relax dependency on Rust library env_logger,
    update Cargo.toml checksum hint file,
    and update (build-)dependencies on librust-env-logger-*-dev;
    this closes: bug#998345

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Nov 2021 19:18:33 +0100

rust-quickcheck (0.9.2-1+apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Mon, 24 Jan 2022 20:49:54 +0000

rust-quickcheck (0.9.2-1) unstable; urgency=medium

  * Package quickcheck 0.9.2 from crates.io using debcargo 2.4.2
  * Drop obsolete patch for dependency version relaxation

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Sun, 05 Apr 2020 18:33:58 +0200

rust-quickcheck (0.9.0-2) unstable; urgency=medium

  * Team upload.
  * Package quickcheck 0.9.0 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Wed, 08 Jan 2020 23:30:38 +0000

rust-quickcheck (0.9.0-1) unstable; urgency=medium

  * Package quickcheck 0.9.0 from crates.io using debcargo 2.4.0

 -- kpcyrd <git@rxv.cc>  Sat, 07 Sep 2019 13:54:58 +0200

rust-quickcheck (0.8.1-1) unstable; urgency=medium

  * Package quickcheck 0.8.1 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Tue, 05 Feb 2019 17:37:59 +0100

rust-quickcheck (0.8.0-1) unstable; urgency=medium

  * Team upload.
  * Package quickcheck 0.8.0 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Tue, 22 Jan 2019 18:53:28 -0800

rust-quickcheck (0.7.2-2) unstable; urgency=medium

  * Team upload.
  * Package quickcheck 0.7.2 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Tue, 18 Dec 2018 17:32:29 -0800

rust-quickcheck (0.7.2-1) unstable; urgency=medium

  * Package quickcheck 0.7.2 from crates.io using debcargo 2.2.9

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Sat, 15 Dec 2018 22:22:21 +0100

rust-quickcheck (0.7.1-1) unstable; urgency=medium

  * Package quickcheck 0.7.1 from crates.io using debcargo 2.2.7

 -- kpcyrd <git@rxv.cc>  Fri, 21 Sep 2018 23:23:46 +0200
